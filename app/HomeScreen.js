import React from 'react';
import {
  Button, View, Text, Image, StyleSheet, AsyncStorage,
  ScrollView, SafeAreaView, Platform, TouchableOpacity,
  StatusBar, Dimensions, ImageBackground, Modal, TouchableHighlight,
  Alert, ActivityIndicator
} from 'react-native';
import { Header, Card } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Entypo';
const width = Dimensions.get("screen").width;
const height = Dimensions.get("screen").height;
export default class MyHomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      servers: [],
      serverCaterogies: [],
      modalVisible: false,
      data: [],
    };
  }

  getFilteredList(serverList, categoryName) {
    let data = [];
    serverList.forEach(server => {
      if (server.category == categoryName) {
        data.push(server);
      }
    });
    return data;
  }

  onChangeData(categoryName) {
    let data = this.getFilteredList(this.state.servers, categoryName);
    this.setState({ data: data });
  }

  componentDidMount() {
    setTimeout(() => { this.scrollView.scrollTo({ x: -30 }) }, 1) // scroll view position fix
  }

  async componentWillMount() {
    let categories = [];
    function checkIfCategoryExists(serverList, categoryName) {
      let status = false;
      categories.forEach(category => {
        if (category.category === categoryName) {
          status = true;
        };
      });
      return status;
    };

    await AsyncStorage.getItem('servers')
      .then((servers) => {
        newCategory = {};
        const serversList = servers ? JSON.parse(servers) : [];
        serversList.forEach(server => {
          if (!checkIfCategoryExists(serversList, server.category)) {
            categories.push({ 'category': server.category });
          };
        });
        let data = this.getFilteredList(serversList, serversList[0].category);
        this.setState({ servers: serversList, serverCaterogies: categories, data: data });
        AsyncStorage.setItem('servers', JSON.stringify(serversList));
      });
  }


  static navigationOptions = {
    drawerLabel: 'Home Screen',
    drawerIcon: ({ tintColor }) => (
      <Icon
        size={20}
        color="#00897B"
        name='home' />
    ),
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  pingServer(ip, port) {
    this.setModalVisible(true);
    let ipa = `http://${ip}:${port}`;
    fetch(ipa)
      .then((response) => {
        if (response.status === 200) {
          Alert.alert(
            'Status',
            'Server is UP',
            [
              { text: 'OK', onPress: () => this.setModalVisible(!this.state.modalVisible) },
            ],
            { cancelable: false }
          )
        } else if (response.status === 404) {
          Alert.alert(
            'Status',
            'Server is Down',
            [
              { text: 'OK', onPress: () => this.setModalVisible(!this.state.modalVisible) },
            ],
            { cancelable: false }
          )
        }
        console.log(response);
      })
      .catch((error) => {
        Alert.alert(
          'Status',
          'Network Error or Server Not Found',
          [
            { text: 'OK', onPress: () => this.setModalVisible(!this.state.modalVisible) },
          ],
          { cancelable: false }
        )
      })
  }

  pingApi(ip, port, api) {
    this.setModalVisible(true);
    let ipa = `http://${ip}:${port}/${api}`;
    fetch(ipa)
      .then((response) => {
        if (response.status === 200) {
          Alert.alert(
            'Status',
            'API working',
            [
              { text: 'OK', onPress: () => this.setModalVisible(!this.state.modalVisible) },
            ],
            { cancelable: false }
          )
        } else if (response.status === 404) {
          Alert.alert(
            'Status',
            'API not working',
            [
              { text: 'OK', onPress: () => this.setModalVisible(!this.state.modalVisible) },
            ],
            { cancelable: false }
          )
        }
        console.log(response);
      })
      .catch((error) => {
        Alert.alert(
          'Status',
          'Network Error or Server Not Found',
          [
            { text: 'OK', onPress: () => this.setModalVisible(!this.state.modalVisible) },
          ],
          { cancelable: false }
        )
      })
  }

  async deleteServers(name) {
    let s = await this.state.servers;
    for (let index = 0; index < s.length; index++) {
      if (s[index].name.toLowerCase() == name) {
        if (index > -1) {
          s.splice(index, 1);
        }
        await this.setState({ servers: s });
        await AsyncStorage.setItem('servers', JSON.stringify(this.state.servers))
          .then(() => { alert('Server Deleted'); this.setState({ searchTerm: '' }); })
          .catch(() => { alert('Error deleting server. \n Try Again') });
        return;
      }
    }
    alert('Server not found');
  }

  render() {
    return (
      <View
        style={{
          backgroundColor: "#fff",
          flex: 1
        }}>
        {/* <View style={{
          height: Platform.OS === 'ios' ? 0 :
            StatusBar.currentHeight, backgroundColor: '#00897B'
        }} /> */}
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            alert('Modal has been closed.');
          }}>
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <View style={{
              flex: 1,
              height: "100%",
              width: "100%",
              backgroundColor: "gray",
              opacity: 0.5,
              position: "absolute",
              top: 0,
              left: 0
            }} />
            <View
              style={{
                width: Dimensions.get("screen").width * (1 / 2),
                height: Dimensions.get("screen").width * (1 / 2),
                backgroundColor: "white",
                borderRadius: 10,
                justifyContent: "center",
                alignItems: "center",
                padding: 10,
                ...Platform.select({
                  ios: {
                    shadowColor: "#afafaf",
                    shadowOffset: {
                      height: 0,
                      width: 0
                    },
                    shadowOpacity: 1,
                    shadowRadius: 30
                  },
                  android: {
                    elevation: 2
                  }
                })
              }}>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          </View>
        </Modal>
        <SafeAreaView style={{ backgroundColor: "#009688" }} />
        <View style={{
          height: 60,
          elevation: 8,
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-around",
          backgroundColor: '#009688'
          // backgroundColor: "#03A9F4"
        }}>
          <Icon
            onPress={() => this.props.navigation.toggleDrawer()}
            name="menu" size={26} style={{ flex: 1, margin: 10 }} color="#fff" />
          <Text style={{ flex: 8, fontSize: 18, color: "#fff", textAlign: "center" }}>Home</Text>
          <View style={{ flex: 1 }} />
        </View>
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <ScrollView
              ref={(scrollView) => { this.scrollView = scrollView; }}
              style={styles.containerCategory}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              decelerationRate={0}
              snapToInterval={width - 60}
              snapToAlignment={"center"}
            >
              {this.state.serverCaterogies.length === 0 ? (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Home');
                  }}
                  style={{ width: 30, height: 30, backgroundColor: "transparent" }}></TouchableOpacity>
              ) : (
                  this.state.serverCaterogies.map((category, i) => {
                    return (
                      <TouchableOpacity onPress={() => this.onChangeData(category.category)} key={i}>

                        <View style={styles.categoryContainer}>
                          <View style={{
                            backgroundColor: "white", flex: 1, borderRadius: 10, padding: 2
                          }}>
                            <View style={{
                              backgroundColor: "#009688", flex: 1, alignItems: "center",
                              padding: 2,
                              justifyContent: "center", borderRadius: 10,
                            }}>
                              <Text style={styles.categoryText}>{category.category}</Text>
                            </View>
                          </View>
                        </View>
                      </TouchableOpacity>
                    );
                  })
                )}
            </ScrollView>
          </View>
          <View style={{ flex: 2.2, justifyContent: 'center', alignItems: 'center', padding: 10 }}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              ref={(scrollView) => { this.scrollView = scrollView; }}
              style={styles.dataContainer}
              decelerationRate={0}
              snapToInterval={width - 60}
              snapToAlignment={"center"}
            >
              {this.state.data.length === 0 ? (
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Home');
                  }}
                  style={{ width: 30, height: 30, backgroundColor: "transparent" }}></TouchableOpacity>
              ) : (
                  this.state.data.map((s, i) => {
                    return (
                      <Card
                        containerStyle={styles.view}
                        borderRadius={10}
                        titleStyle={{ color: "#00897B" }}
                        title={s.name} key={i}>
                        <View>
                          <Text style={{ fontSize: 16, color: "#00897B" }}>
                            <Text style={{ fontWeight: "bold", color: "#00897B" }}>
                              IP Address :
                          </Text>
                            {s.ip}
                          </Text>
                          <Text style={{ fontSize: 16, color: "#00897B" }}>
                            <Text style={{ fontWeight: "bold", color: "#00897B" }}>
                              Public Address :
                          </Text>
                            {s.Public_ip}
                          </Text>
                          <Text style={{ fontSize: 16, color: "#00897B" }}>
                            <Text style={{ fontWeight: "bold", color: "#00897B" }}>
                              Port :
                          </Text>
                            {s.port}
                          </Text>
                        </View>
                        <View style={{
                          flexDirection: "row",
                          width: "100%",
                          justifyContent: "space-around",
                          backgroundColor: "white",
                          marginTop: 10,
                        }}>
                          <TouchableOpacity
                            onPress={() => { this.pingServer(s.ip, s.port) }}
                            style={styles.btn}>
                            <Text style={styles.btnText}>Server</Text>
                          </TouchableOpacity>
                          <TouchableOpacity

                            // delete server button
                            onPress={() => this.deleteServers(s.name)}

                            style={styles.btn}>
                            <Text style={styles.btnText}>Delete</Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            onPress={() => { this.pingApi(s.ip, s.port, s.api) }}
                            style={styles.btn}>
                            <Text style={styles.btnText}>Api</Text>
                          </TouchableOpacity>
                        </View>
                      </Card>
                    );
                  })
                )}
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
  containerCategory: {
    backgroundColor: "white",
    height: 20,
  },
  btn: {
    backgroundColor: "#fff",
    width: 80,
    marginBottom: 5,
    height: 40,
    elevation: 10,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    padding: 5,
    borderBottomWidth: 2.5,
    borderColor: "#00897B",
    borderWidth: 1
  },
  btnText: {
    fontWeight: "bold",
    color: "#00897B"
  },
  view: {
    marginBottom: 10,
    backgroundColor: 'white',
    width: width - 80,
    borderColor: "#ccc",
    borderRightWidth: 2,
    borderLeftWidth: 0,
    borderTopWidth: 0,
    borderBottomWidth: 2,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: {
          height: 2,
          width: 2
        },
        shadowOpacity: 1,
        shadowRadius: 1
      },
      android: {
        elevation: 3
      }
    })
    //paddingHorizontal : 30
  },
  view2: {
    marginTop: 100,
    width: width - 80,
    margin: 10,
    height: 200,
    borderRadius: 10,
    //paddingHorizontal : 30
  },
  categoryContainer: {
    backgroundColor: "#00796B",
    width: width / 1.5,
    margin: 10,
    marginTop: 25,
    height: height / 5,
    padding: 2,
    // borderWidth: 2,
    // borderColor: "white",
    borderRadius: 10,
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: {
          height: 2,
          width: 2
        },
        shadowOpacity: 1,
        shadowRadius: 1
      },
      android: {
        elevation: 5
      }
    })
  },
  categoryText: {
    fontWeight: "100",
    fontSize: 30,
    // color: "#00796B",
    color: "white",

  },
  dataContainer: {
    backgroundColor: "#fff"
  },
  checkButton: {
    width: "100%",
    ...Platform.select({
      ios: {
        shadowColor: "black",
        shadowOffset: {
          height: 2,
          width: 2
        },
        shadowOpacity: 1,
        shadowRadius: 1
      },
      android: {
        elevation: 8
      }
    })
  }
});