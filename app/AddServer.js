import React from 'react';
import {
  Button, View, Text, Image, TextInput, StyleSheet,
  AsyncStorage, Platform, ScrollView, KeyboardAvoidingView,
  SafeAreaView, ImageBackground, StatusBar
} from 'react-native';
import { Header, Card, Input, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Foundation';
import EIcon from 'react-native-vector-icons/Entypo';
export default class AddServer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: null,
      categoryName: null,
      ipAddress: null,
      publicIpAddress: null,
      port: null,
      api: null,

      isValidIP: true,
      isValidPublicIP: true,
      isValidPort: true,
    }
    this.validateIP = this.validateIP.bind(this);
    this.validatePort = this.validatePort.bind(this);
    this.saveServer = this.saveServer.bind(this);
    this.validatePublicIP = this.validatePublicIP.bind(this);
  }

  static navigationOptions = {
    drawerLabel: 'Add Server',
    drawerIcon: ({ tintColor }) => (
      <Icon
        size={20}
        color="#00897B"
        name='plus' />
    ),
  };

  saveServer() {
    if (this.state.isValidIP && this.state.isValidPort) {
      AsyncStorage.getItem('servers')
        .then((server) => {
          let items = server ? JSON.parse(server) : [];
          let newServer = { 'name': this.state.name, 'category': this.state.categoryName, 'ip': this.state.ipAddress,'Public_ip': this.state.publicIpAddress, 'port': this.state.port, 'api': this.state.api };
          items.unshift(newServer);
          AsyncStorage.setItem('servers', JSON.stringify(items)).then(() => { [alert('New Server added'), this.props.navigation.navigate('Home')] }).catch(() => { alert('Error adding server') }).done();
        })
        .catch((error) => {
          console.log('Error fetching items')
        })
        .done();

      //After adding server  go to home page 
      // this.props.navigation.navigate('Home');
    }
  }

  validateIP(ipa) {
    const ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    if (ipa.match(ipformat)) {
      this.setState({ 'ipAddress': ipa, 'isValidIP': true });
      return true;
    } else {
      this.setState({ 'ipAddress': '', 'isValidIP': false });
    }
    return false;
  }

  validatePublicIP(ipa) {
    const ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    if (ipa.match(ipformat)) {
      this.setState({ 'publicIpAddress': ipa, 'isValidPublicIP': true });
      return true;
    } else {
      this.setState({ 'publicIpAddress': '', 'isValidPublicIP': false });
    }
    return false;
  }

  validatePort(p) {
    if (p > 0 && p < 65535) {
      this.setState({ 'port': p, 'isValidPort': true });
      return true;
    } else {
      this.setState({ 'port': '', 'isValidPort': false });
    }
    return false;
  }

  render() {
    return (
      <KeyboardAvoidingView keyboardVerticalOffset={-500}
        behavior="padding" 
        style={{ flex: 1 }}
      >
        <View style={{ flex: 1 }}>
          <SafeAreaView style={{ backgroundColor: "#009688" }} />
          <View style={{
            height: 60,
            elevation: 8,
            width: "100%",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-around",
            backgroundColor: "#009688"
          }}>
            <EIcon
              onPress={() => this.props.navigation.toggleDrawer()}
              name="menu" size={26} style={{ flex: 1, margin: 10 }} color="#fff" />
            <Text style={{ flex: 8, fontSize: 18, color: "#fff", textAlign: "center" }}>Add Servers</Text>
            <View style={{ flex: 1 }} />
          </View>
          <ScrollView
            contentContainerStyle={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "transparent"
            }}
            showsVerticalScrollIndicator={false}>
            <Card title='Add Server'
              containerStyle={{
                opacity: 0.7,
                maxWidth: 350,
                ...Platform.select({
                  ios: {
                    shadowColor: "#1a1a1a",
                    shadowOffset: {
                      height: 2,
                      width: 2
                    },
                    shadowOpacity: 1,
                    shadowRadius: 2
                  },
                  android: {
                    elevation: 2
                  }
                })
              }}
              borderRadius={10}
            >
              <FormLabel><Text style={{ fontSize: 14, color: "#666666" }}>Name of Server</Text></FormLabel>
              <FormInput
                placeholder="Enter SERVER name"
                onChangeText={(name) => { this.setState({ name }) }} />
              <FormLabel><Text style={{ fontSize: 14, color: "#666666" }}>Category Name</Text></FormLabel>
              <FormInput
                placeholder="Enter CATEGORY name"
                onChangeText={(categoryName) => { this.setState({ categoryName }) }} />
              <FormLabel><Text style={{ fontSize: 14, color: "#666666" }}>IP Address</Text></FormLabel>
              {!this.state.isValidIP && <FormValidationMessage>Invalid IP Address</FormValidationMessage>}
              <FormInput
                placeholder="Enter IP Address"
                keyboardType="numeric"
                maxLength={7}
                onChangeText={(ipAddress) => { this.validateIP(ipAddress) }} />
              
              <FormLabel><Text style={{ fontSize: 14, color: "#666666" }}>Public IP Address</Text></FormLabel>
              {!this.state.isValidPublicIP && <FormValidationMessage>Invalid Public IP Address</FormValidationMessage>}
              <FormInput
                placeholder="Enter Public IP Address"
                keyboardType="numeric"
                maxLength={7}
                onChangeText={(publicIpAddress) => { this.validatePublicIP(publicIpAddress) }} />
              
              <FormLabel><Text style={{ fontSize: 14, color: "#666666" }}>Port</Text> </FormLabel>
              {!this.state.isValidPort ? <FormValidationMessage>Invalid Port Address</FormValidationMessage> : null}
              <FormInput
                placeholder="Enter PORT number"
                keyboardType="numeric"
                onChangeText={(port) => { this.validatePort(port) }} />
              <FormLabel><Text style={{ fontSize: 14, color: "#666666" }}>API</Text></FormLabel>
              <FormInput
                placeholder="Enter API"
                onChangeText={(api) => { this.setState({ api: api }) }} />
              {this.state.ipAddress === null || this.state.port === null || this.state.categoryName === null || this.state.name === null ?
                <Button title='Add' disabled onPress={() => { console.log("Clicked") }} /> : <Button title='Add' onPress={() => { this.saveServer() }} />
              }
              {/* {(this.state.isValidIP && this.state.isValidPort) ? <Button title='Add' onPress={() => { this.saveServer() }} /> : <Button title='Add' disabled />} */}
            </Card>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>

    );
  }
}
const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
  btn: {
    backgroundColor: "#fafafa",
    width: 80,
    height: 40,
    elevation: 8,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    padding: 5
  },
});

