import React from 'react';
import { Button, View, Text, Image, StyleSheet, AsyncStorage, ImageBackground, StatusBar, Platform, TouchableOpacity, SafeAreaView } from 'react-native';
import { Header, SearchBar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import EIcon from 'react-native-vector-icons/Entypo';

export default class DeleteServer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      servers: [],
      searchTerm: '',
    }
  }

  componentWillMount() {
    AsyncStorage.getItem('servers')
      .then((server) => {
        const c = server ? JSON.parse(server) : [];
        this.setState({ servers: c });
      })
      .catch(() => { alert('Error fetching servers') });
  }

  static navigationOptions = {
    drawerLabel: 'Delete Server',
    drawerIcon: ({ tintColor }) => (
      <Icon
        size={20}
        color="#00897B"
        name='delete' />
    ),
  };


  async searchInServers() {
    let s = await this.state.servers;
    for (let index = 0; index < s.length; index++) {
      if (s[index].name.toLowerCase() == this.state.searchTerm.toLowerCase()) {
        if (index > -1) {
          s.splice(index, 1);
        }
        await this.setState({ servers: s });
        await AsyncStorage.setItem('servers', JSON.stringify(this.state.servers))
          .then(() => { alert('Server Deleted'); this.setState({ searchTerm: '' }); })
          .catch(() => { alert('Error deleting server. \n Try Again') });
        return;
      }
    }
    alert('Server not found');
  }

  render() {
    return (
      <View>
        {/* <View style={{ height: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight, backgroundColor: '#00897B' }} /> */}
        {/* Header */}
        <SafeAreaView style={{ backgroundColor: "#009688" }} />
        <View style={{
          height: 60,
          elevation: 8,
          width: "100%",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-around",
          backgroundColor: "#009688"
        }}>
          <EIcon
            onPress={() => this.props.navigation.toggleDrawer()}
            name="menu" size={26} style={{ flex: 1, margin: 10 }} color="#fff" />
          <Text style={{ flex: 8, fontSize: 18, color: "#fff", textAlign: "center" }}>Delete Server</Text>
          <View style={{ flex: 1 }} />
        </View>
        {/* Header */}
        <SearchBar
          lightTheme
          containerStyle={{ backgroundColor: "white" }}
          onChangeText={(term) => { this.setState({ searchTerm: term }) }}
          onClearText={() => { this.setState({ searchTerm: '' }) }}
          placeholder='Search...' />

        <View style={{ width: "100%", justifyContent: "center", alignItems: "center", padding: 20 }}>
          <TouchableOpacity
            onPress={() => { this.searchInServers() }}
            style={styles.btn}>
            <Text style={styles.btnText}>Delete</Text>
          </TouchableOpacity>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24,
  },
  btn: {
    backgroundColor: "#66B3FF",
    width: "100%",
    height: 40,
    elevation: 8,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    padding: 5,
    margin: 10
  },
  btnText: {
    fontWeight: "bold",
    color: "white"
  },
});