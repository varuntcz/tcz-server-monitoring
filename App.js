import React from 'react';
import { StyleSheet, Text, View, Button, Headers, StatusBar, Platform, AsyncStorage, ImageBackground } from 'react-native';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation';
import { Header } from 'react-native-elements';
import MyHomeScreen from './app/HomeScreen';
import DeleteServer from './app/DeleteServer';
import AddServer from './app/AddServer';

const AppNav = createDrawerNavigator({
  Home: {
    screen: MyHomeScreen,
  },
  AddServer: {
    screen: AddServer
  },
  Delete: {
    screen: DeleteServer,
  },
},
  {
    contentOptions: {
      inactiveTintColor: '#009688',
      activeTintColor: '#009688'
    },
    drawerBackgroundColor: '#ffff',
  }
);

export default class App extends React.Component {
  render() {
    console.disableYellowBox = true;
    return (

      <AppNav />


    );

  }
}